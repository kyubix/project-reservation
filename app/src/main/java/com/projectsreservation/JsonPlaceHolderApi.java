package com.projectsreservation;

import android.util.Base64;

import com.projectsreservation.Models.Auth.LoginResponse;
import com.projectsreservation.Models.Auth.RegisterRequest;
import com.projectsreservation.Models.Auth.RegisterResponse;
import com.projectsreservation.Models.Projects.ProjectBaseModel;
import com.projectsreservation.Models.Projects.ProjectListResponse;
import com.projectsreservation.Models.User.UserInfoResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {
    @POST("api/user/register")
    Call<RegisterResponse> registerUser(@Body RegisterRequest request);

    @FormUrlEncoded
    @Headers({"Authorization: Basic Y2xpZW50SWQ6Y2xpZW50U2VjcmV0"})
    @POST("oauth/token")
    Call<LoginResponse> generateToken(@Field("grant_type") String grantType,
                                      @Field("username") String username,
                                      @Field("password") String password);

    @GET("api/user/current")
    Call<UserInfoResponse> getUserInfo(@Header("Authorization") String token);

    @GET("api/project")
    Call<ProjectListResponse> getProjectList(@Header("Authorization") String token,
                                             @Query("page") int page,
                                             @Query("size") int size);

    @GET("api/project/all")
    Call<ProjectListResponse> getProjectList(@Header("Authorization") String token);

    @POST("api/project")
    Call<ProjectBaseModel> createProject(@Header("Authorization") String token,
                                         @Body ProjectBaseModel request);

    @PUT("api/project/reserve/{id}")
    Call<ProjectBaseModel> reserveProject(@Header("Authorization") String token,
                                           @Path("id") int id);

    @DELETE("api/project/delete/{id}")
    Call<ProjectBaseModel> removeProject(@Header("Authorization") String token,
                                          @Path("id") int id);

    @GET("api/users")
    Call<List<UserInfoResponse>> getUsersWithReservedProjects(@Header("Authorization") String token);

}
