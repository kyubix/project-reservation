package com.projectsreservation.Helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.projectsreservation.Activities.ProjectDetailsActivity;
import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.Auth.UserInfoStorage;
import com.projectsreservation.Models.Projects.Category;
import com.projectsreservation.Models.Projects.ProjectListItem;
import com.projectsreservation.R;

import java.util.ArrayList;
import java.util.List;

public class ProjectListAdapter extends BaseAdapter {

    private List<ProjectListItem> list = new ArrayList<ProjectListItem>();
    private Context context;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    public ProjectListAdapter(List<ProjectListItem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ProjectListItem getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).getId();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.project_list_layout, null);
        }

        TextView listItemText = view.findViewById(R.id.list_item_string);
        listItemText.setText(list.get(position).getTitle());

        TextView listItemNumber = view.findViewById(R.id.list_item_number);
        listItemNumber.setText(list.get(position).getAvailableReservations() + "/5");

        if (list.get(position).getAvailableReservations() == 0) {
            listItemText.setTextColor(Color.argb(0xFF, 0xDA, 0x07, 0x07));
            listItemNumber.setTextColor(Color.argb(0xFF, 0xDA, 0x07, 0x07));
            //listItemText.setTextColor(R.color.colorDanger);
            //listItemNumber.setTextColor(R.color.colorDanger);
        } else {
            listItemText.setTextColor(Color.argb(0xFF, 0x30, 0x52, 0xB9));
            listItemNumber.setTextColor(Color.argb(0xFF, 0x30, 0x52, 0xB9));
            //listItemText.setTextColor(R.color.colorAccent);
            //listItemNumber.setTextColor(R.color.colorAccent);
        }

        RelativeLayout listItemLayout = view.findViewById(R.id.project_list_item_layout);

        listItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getApplicationContext(), ProjectDetailsActivity.class);
                intent.putExtra("id", list.get(position).getId());
                intent.putExtra("title", list.get(position).getTitle());
                intent.putExtra("description", list.get(position).getDescription());
                intent.putExtra("available_reservations", list.get(position).getAvailableReservations());

                List<Category> categories = list.get(position).getCategories();
                for (int i = 0;  i < categories.size(); i++) {
                    intent.putExtra("category" + i, categories.get(i).getName());
                }
                context.startActivity(intent);
            }
        });

        return view;
    }


}
