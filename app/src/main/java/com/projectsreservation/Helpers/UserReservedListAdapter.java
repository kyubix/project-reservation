package com.projectsreservation.Helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.User.UserInfoResponse;
import com.projectsreservation.R;

import java.util.ArrayList;
import java.util.List;

public class UserReservedListAdapter extends BaseAdapter {

    private List<UserInfoResponse> list = new ArrayList<UserInfoResponse>();
    private Context context;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    public UserReservedListAdapter(List<UserInfoResponse> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public UserInfoResponse getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return list.get(pos).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.user_reserved_list_layout, null);
        }

        TextView listItemText = view.findViewById(R.id.user_reserved_list_item_project);
        listItemText.setText(list.get(position).getProject().getTitle());

        TextView listItemNumber = view.findViewById(R.id.user_reserved_list_item_user);
        listItemNumber.setText(list.get(position).getUsername());

        return view;
    }
}
