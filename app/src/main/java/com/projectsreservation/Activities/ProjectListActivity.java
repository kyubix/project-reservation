package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.projectsreservation.Helpers.ProjectListAdapter;
import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.TokenStorage;
import com.projectsreservation.Models.Auth.UserInfoStorage;
import com.projectsreservation.Models.Projects.ProjectListItem;
import com.projectsreservation.Models.Projects.ProjectListResponse;
import com.projectsreservation.Models.User.UserInfoResponse;
import com.projectsreservation.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectListActivity extends AppCompatActivity {

    private ListView listView;
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private ProjectListAdapter adapter;
    private FloatingActionButton btnPlus;
    private FloatingActionButton btnReserved;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);

        listView = findViewById(R.id.project_list);
        btnPlus = findViewById(R.id.button_plus);
        btnReserved = findViewById(R.id.button_reservations);
        progressBar = findViewById(R.id.project_list_loading);

        if(UserInfoStorage.role.equals(ApiProperties.ROLE_USER)) {
            btnPlus.hide();
            btnPlus.setClickable(false);
            btnReserved.hide();
            btnReserved.setClickable(false);
        }

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                openProjectAddActivity();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        btnReserved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                openUserReservedListActivity();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        getProjectList();
    }

    private void getProjectList() {
        progressBar.setVisibility(View.VISIBLE);
        Call<ProjectListResponse> call = jsonPlaceHolderApi.getProjectList(TokenStorage.bearerToken, 0, 200);

        call.enqueue(new Callback<ProjectListResponse>() {
            @Override
            public void onResponse(Call<ProjectListResponse> call, Response<ProjectListResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<ProjectListItem> responseList = response.body().getContent();
                        writeListView(responseList);

                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Niepowodzenie - wystąpił błąd",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void onFailure(Call<ProjectListResponse> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Niepowodzenie - wystąpił błąd",
                        Toast.LENGTH_SHORT);
                toast.show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void writeListView(List<ProjectListItem> projects) {
        adapter = new ProjectListAdapter(projects, this);
        listView.setAdapter(adapter);
    }

    private void openProjectAddActivity()
    {
        Intent intent = new Intent(this, ProjectAddActivity.class);
        startActivity(intent);
    }

    private void openUserReservedListActivity()
    {
        Intent intent = new Intent(this, UserReservedListActivity.class);
        startActivity(intent);
    }
}
