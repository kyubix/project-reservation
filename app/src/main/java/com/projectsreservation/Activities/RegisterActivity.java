package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.RegisterRequest;
import com.projectsreservation.Models.Auth.RegisterResponse;
import com.projectsreservation.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    private Button registerButton;
    private EditText usernameText;
    private EditText passwordText;
    private EditText matchingPasswordText;
    private TextView textView;
    private ProgressBar progressBar;
    private JsonPlaceHolderApi jsonPlaceHolderApi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        registerButton = findViewById(R.id.register_submit);
        usernameText = findViewById(R.id.register_username);
        passwordText = findViewById(R.id.register_password);
        matchingPasswordText = findViewById(R.id.register_matching_password);
        textView = findViewById(R.id.register_message);
        progressBar = findViewById(R.id.register_loading);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                RegisterRequest request = new RegisterRequest();
                request.setUsername(usernameText.getText().toString());
                request.setPassword(passwordText.getText().toString());
                request.setMatchingPassword(matchingPasswordText.getText().toString());

                registerUser(request);
            }
        });
    }

    private void registerUser(RegisterRequest request)
    {
        try {
            Call<RegisterResponse> call = jsonPlaceHolderApi.registerUser(request);
            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 500 || response.code() == 400) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Niepoprawnie wypełniony formularz",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Niepowodzenie - wystąpił błąd",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    progressBar.setVisibility(View.GONE);
                    return;
                }
                    RegisterResponse user = response.body();
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Zarejestrowano użytkownika: " + user.getUsername(),
                            Toast.LENGTH_SHORT);
                    toast.show();
                    openMainActivity();
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Niepowodzenie - wystąpił błąd",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        catch (Exception e)
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Niepowodzenie - wystąpił błąd",
                    Toast.LENGTH_SHORT);
            toast.show();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void openMainActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
