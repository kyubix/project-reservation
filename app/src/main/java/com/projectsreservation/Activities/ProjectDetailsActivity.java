package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.TokenStorage;
import com.projectsreservation.Models.Auth.UserInfoStorage;
import com.projectsreservation.Models.Projects.Category;
import com.projectsreservation.Models.Projects.ProjectBaseModel;
import com.projectsreservation.R;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectDetailsActivity extends AppCompatActivity {

    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private TextView titleContent;
    private TextView descriptionContent;
    private TextView categoriesContent;
    private TextView infoText;
    private Button reserveButton;
    private Button removeButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);

        titleContent = findViewById(R.id.project_details_title_content);
        String title = getIntent().getSerializableExtra("title").toString();
        titleContent.setText(title);

        descriptionContent = findViewById(R.id.project_details_description_content);
        String description = getIntent().getSerializableExtra("description").toString();
        descriptionContent.setText(description);

        categoriesContent = findViewById(R.id.project_details_categories_content);
        int i = 0;
        while (true) {
            if (getIntent().getSerializableExtra("category" + i) == null) {
                break;
            }
            String category = getIntent().getSerializableExtra("category" + i).toString();
            categoriesContent.append(category.toUpperCase() + "\n");
            i++;
        }

        String idString = getIntent().getSerializableExtra("id").toString();
        final int id = Integer.parseInt(idString);
        String availableReservationsString = getIntent().getSerializableExtra("available_reservations").toString();
        int availableReservations = Integer.parseInt(availableReservationsString);

        reserveButton = findViewById(R.id.project_details_reserve);
        removeButton = findViewById(R.id.project_details_remove);
        infoText = findViewById(R.id.project_details_info);
        progressBar = findViewById(R.id.project_details_loading);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        if (UserInfoStorage.role.equals(ApiProperties.ROLE_USER)) {
            removeButton.setVisibility(View.GONE);
            removeButton.setClickable(false);
            if (availableReservations > 0) {
                reserveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setVisibility(View.VISIBLE);
                        reserveProject(id);
                    }
                });
            }
            else {
                reserveButton.setVisibility(View.GONE);
                reserveButton.setClickable(false);
                infoText.setVisibility(View.VISIBLE);
                infoText.setText("Nie można już rezerwować tego projektu");
            }
        }
        else if (UserInfoStorage.role.equals(ApiProperties.ROLE_ADMIN))
        {
            reserveButton.setVisibility(View.GONE);
            reserveButton.setClickable(false);
            if (availableReservations == 5)
            {
                removeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setVisibility(View.VISIBLE);
                        removeProject(id);
                    }
                });
            }
            else
            {
                removeButton.setVisibility(View.GONE);
                removeButton.setClickable(false);
                infoText.setVisibility(View.VISIBLE);
                infoText.setText("Nie można już usunąć tego projektu - ktoś go zarezerwował");
            }
        }
    }

    private void reserveProject(int id)
    {
        Call<ProjectBaseModel> call = jsonPlaceHolderApi.reserveProject(TokenStorage.bearerToken, id);
        call.enqueue(new Callback<ProjectBaseModel>() {
            @Override
            public void onResponse(Call<ProjectBaseModel> call, Response<ProjectBaseModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        ProjectBaseModel projectResponse = response.body();
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Zarezerwowano projekt",
                                Toast.LENGTH_SHORT);
                        toast.show();
                        openProjectReservedActivity(projectResponse);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
                else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Niepowodzenie - nie można zarezerwować projektu",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    progressBar.setVisibility(View.INVISIBLE);
                    }
                }

            @Override
            public void onFailure(Call<ProjectBaseModel> call, Throwable t) {
                infoText.setText(t.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void removeProject(int id)
    {
        Call<ProjectBaseModel> call = jsonPlaceHolderApi.removeProject(TokenStorage.bearerToken, id);
        call.enqueue(new Callback<ProjectBaseModel>() {
            @Override
            public void onResponse(Call<ProjectBaseModel> call, Response<ProjectBaseModel> response) {
                if (response.isSuccessful()) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Usunięto projekt",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    openProjectListActivity();
                    progressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Niepowodzenie - nie można usunąć projektu",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ProjectBaseModel> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Nie udało się usunąć projektu: " + t.getMessage(),
                        Toast.LENGTH_SHORT);
                toast.show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void openProjectListActivity()
    {
        Intent intent = new Intent(this, ProjectListActivity.class);
        startActivity(intent);
    }

    private void openProjectReservedActivity(ProjectBaseModel project)
    {
        Intent intent = new Intent(this, ProjectReservedActivity.class);
        intent.putExtra("title", project.getTitle());
        intent.putExtra("description", project.getDescription());

        List<Category> categories = project.getCategories();
        for (int i = 0;  i < categories.size(); i++) {
            intent.putExtra("category" + i, categories.get(i).getName());
        }
        startActivity(intent);
    }
}
