package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.LoginRequest;
import com.projectsreservation.Models.Auth.LoginResponse;
import com.projectsreservation.Models.Auth.TokenStorage;
import com.projectsreservation.Models.Auth.UserInfoStorage;
import com.projectsreservation.Models.Projects.Category;
import com.projectsreservation.Models.Projects.ProjectBaseModel;
import com.projectsreservation.Models.User.UserInfoResponse;
import com.projectsreservation.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText usernameText;
    private EditText passwordText;
    private TextView textView;
    private ProgressBar progressBar;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        loginButton = findViewById(R.id.login_submit);
        usernameText = findViewById(R.id.login_username);
        passwordText = findViewById(R.id.login_password);
        textView = findViewById(R.id.login_message);
        progressBar = findViewById(R.id.login_loading);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                LoginRequest request = new LoginRequest();
                request.setUsername(usernameText.getText().toString());
                request.setPassword(passwordText.getText().toString());

                login(request);
            }
        });
    }

    private void login(LoginRequest request)
    {
        try {
            Call<LoginResponse> call = jsonPlaceHolderApi.generateToken(
                    "password",
                    request.getUsername(),
                    request.getPassword());
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (!response.isSuccessful()) {
                        if (response.code() == 500 || response.code() == 400) {
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Niepoprawny login lub hasło",
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        else {
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "Niepowodzenie - wystąpił błąd",
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        progressBar.setVisibility(View.GONE);
                        return;
                    }
                    LoginResponse responseToken = response.body();
                    TokenStorage.bearerToken = "Bearer " + responseToken.getAccessToken();
                    getUserInfo();
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Zalogowano",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    textView.setText(t.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
        catch (Exception e)
        {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Niepowodzenie - wystąpił błąd",
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void getUserInfo() {
        Call<UserInfoResponse> call = jsonPlaceHolderApi.getUserInfo(TokenStorage.bearerToken);
        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                if (!response.isSuccessful()) {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Niepowodzenie - wystąpił błąd",
                            Toast.LENGTH_SHORT);
                    toast.show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                UserInfoResponse userInfo = response.body();
                UserInfoStorage.id = userInfo.getId();
                UserInfoStorage.username = userInfo.getUsername();
                UserInfoStorage.role = userInfo.getRole();

                if (userInfo.getRole().equals(ApiProperties.ROLE_USER)
                && userInfo.getProject().getId() != null)
                {
                    openProjectReservedActivity(userInfo.getProject());
                }
                else {
                    openProjectListActivity();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Niepowodzenie - wystąpił błąd",
                        Toast.LENGTH_SHORT);
                toast.show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void openProjectListActivity()
    {
        Intent intent = new Intent(this, ProjectListActivity.class);
        startActivity(intent);
    }

    private void openProjectReservedActivity(ProjectBaseModel project)
    {
        Intent intent = new Intent(this, ProjectReservedActivity.class);
        intent.putExtra("title", project.getTitle());
        intent.putExtra("description", project.getDescription());

        List<Category> categories = project.getCategories();
        for (int i = 0;  i < categories.size(); i++) {
            intent.putExtra("category" + i, categories.get(i).getName());
        }
        startActivity(intent);
    }
}
