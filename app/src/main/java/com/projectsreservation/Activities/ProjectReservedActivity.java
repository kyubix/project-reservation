package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectReservedActivity extends AppCompatActivity {

    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private TextView titleContent;
    private TextView descriptionContent;
    private TextView categoriesContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_reserved);

        titleContent = findViewById(R.id.project_reserved_title_content);
        String title = getIntent().getSerializableExtra("title").toString();
        titleContent.setText(title);

        descriptionContent = findViewById(R.id.project_reserved_description_content);
        String description = getIntent().getSerializableExtra("description").toString();
        descriptionContent.setText(description);

        categoriesContent = findViewById(R.id.project_reserved_categories_content);
        int i = 0;
        while (true) {
            if (getIntent().getSerializableExtra("category" + i) == null) {
                break;
            }
            String category = getIntent().getSerializableExtra("category" + i).toString();
            categoriesContent.append(category.toUpperCase() + "\n");
            i++;
        }
    }
}
