package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.TokenStorage;
import com.projectsreservation.Models.Projects.Category;
import com.projectsreservation.Models.Projects.ProjectBaseModel;
import com.projectsreservation.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectAddActivity extends AppCompatActivity {

    private Button createButton;
    private EditText titleText;
    private EditText descriptionText;
    private EditText categoriesText;
    private TextView textView;
    private ProgressBar progressBar;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_add);

        titleText = findViewById(R.id.project_add_title);
        descriptionText = findViewById(R.id.project_add_description);
        categoriesText = findViewById(R.id.project_add_categories);
        createButton = findViewById(R.id.project_add_submit);
        textView = findViewById(R.id.project_add_message);
        progressBar = findViewById(R.id.project_add_loading);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                List<Category> categories = new ArrayList<>();
                List<String> categoryStrings = Arrays.asList(categoriesText.getText().toString().split(" "));
                for (String categoryString : categoryStrings)
                    categories.add(new Category(categoryString));

                ProjectBaseModel request = new ProjectBaseModel(
                        titleText.getText().toString(),
                        descriptionText.getText().toString(),
                        categories);

                createProject(request);
            }
        });


    }

    private void createProject(ProjectBaseModel request)
    {
        Call<ProjectBaseModel> call = jsonPlaceHolderApi.createProject(TokenStorage.bearerToken, request);
        call.enqueue(new Callback<ProjectBaseModel>() {
            @Override
            public void onResponse(Call<ProjectBaseModel> call, Response<ProjectBaseModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        ProjectBaseModel responseModel = response.body();
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Dodano nowy projekt",
                                Toast.LENGTH_SHORT);
                        toast.show();
                        openProjectListActivity();
                        progressBar.setVisibility(View.GONE);
                    }
                }
                else {
                    if (response.code() == 500) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Identyczny tytuł lub temat już istnieje",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else if (response.code() == 400) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Uzupełnij poprawnie formularz",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    else
                    {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "Niepowodzenie - wystąpił błąd",
                                Toast.LENGTH_SHORT);
                        toast.show();
                        textView.setText(response.code());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProjectBaseModel> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Niepowodzenie - wystąpił błąd",
                        Toast.LENGTH_SHORT);
                toast.show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void openProjectListActivity()
    {
        Intent intent = new Intent(this, ProjectListActivity.class);
        startActivity(intent);
    }
}
