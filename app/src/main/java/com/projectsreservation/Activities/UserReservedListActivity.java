package com.projectsreservation.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projectsreservation.Helpers.ProjectListAdapter;
import com.projectsreservation.Helpers.UserReservedListAdapter;
import com.projectsreservation.JsonPlaceHolderApi;
import com.projectsreservation.Models.ApiProperties;
import com.projectsreservation.Models.Auth.TokenStorage;
import com.projectsreservation.Models.Projects.ProjectListItem;
import com.projectsreservation.Models.User.UserInfoResponse;
import com.projectsreservation.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserReservedListActivity extends AppCompatActivity {

    private ListView listView;
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private UserReservedListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reserved_list);

        listView = findViewById(R.id.user_list_list_view);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiProperties.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        getReservationList();

    }

    private void getReservationList()
    {
        Call<List<UserInfoResponse>> call = jsonPlaceHolderApi.getUsersWithReservedProjects(TokenStorage.bearerToken);
        call.enqueue(new Callback<List<UserInfoResponse>>() {
            @Override
            public void onResponse(Call<List<UserInfoResponse>> call, Response<List<UserInfoResponse>> response) {
                if (response.isSuccessful())
                {
                    List<UserInfoResponse> reservationList = response.body();
                    writeListView(reservationList);
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Niepowodzenie - wystąpił błąd",
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Call<List<UserInfoResponse>> call, Throwable t) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Niepowodzenie - wystąpił błąd",
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void writeListView(List<UserInfoResponse> reservations) {
        adapter = new UserReservedListAdapter(reservations, this);
        listView.setAdapter(adapter);
    }
}
