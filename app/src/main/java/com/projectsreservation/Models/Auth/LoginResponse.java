package com.projectsreservation.Models.Auth;

public class LoginResponse {
    private String access_token;

    public String getAccessToken() {
        return access_token;
    }
}
