package com.projectsreservation.Models;

public class ApiProperties {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    // Dom
    //public static final String API_URL = "http://192.168.100.7:8080/";
    // Solidarnosci
    //public static final String API_URL = "http://192.168.1.17:8080/";
    // Hotspot
    public static final String API_URL = "http://192.168.43.75:8080/";
    // Akademik
    //public static final String API_URL = "http://192.168.1.102:8080/";
}
