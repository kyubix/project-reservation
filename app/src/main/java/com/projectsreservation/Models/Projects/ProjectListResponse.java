package com.projectsreservation.Models.Projects;

import java.util.List;

public class ProjectListResponse {
    private List<ProjectListItem> content;

    public List<ProjectListItem> getContent() {
        return content;
    }
}
