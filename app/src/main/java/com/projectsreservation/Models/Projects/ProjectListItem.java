package com.projectsreservation.Models.Projects;

import java.util.List;

public class ProjectListItem {
    private Integer id;
    private String title;
    private String description;
    private Integer availableReservations;
    private List<Category> categories;

    public ProjectListItem(String title, String description)
    {
        this.title = title;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getAvailableReservations() {
        return availableReservations;
    }

    public List<Category> getCategories() {
        return categories;
    }


}
