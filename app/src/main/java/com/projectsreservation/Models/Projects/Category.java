package com.projectsreservation.Models.Projects;

import java.io.Serializable;

public class Category implements Serializable {
    private Integer id;
    private String name;

    public Category(String name)
    {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
