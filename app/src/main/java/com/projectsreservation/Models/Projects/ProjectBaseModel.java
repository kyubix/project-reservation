package com.projectsreservation.Models.Projects;

import java.util.List;

public class ProjectBaseModel {
    private Integer id;
    private String title;
    private String description;
    private String availableReservations;
    private List<Category> categories;

    public ProjectBaseModel(String title, String description, String availableReservations, List<Category> categories) {
        this.title = title;
        this.description = description;
        this.availableReservations = availableReservations;
        this.categories = categories;
    }

    public ProjectBaseModel(String title, String description, List<Category> categories) {
        this.title = title;
        this.description = description;
        this.categories = categories;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAvailableReservations() {
        return availableReservations;
    }

    public List<Category> getCategories() {
        return categories;
    }
}
