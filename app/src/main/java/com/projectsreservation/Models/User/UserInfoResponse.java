package com.projectsreservation.Models.User;

import com.projectsreservation.Models.Projects.ProjectBaseModel;

public class UserInfoResponse {
    private Integer id;
    private String username;
    private String role;
    private ProjectBaseModel project;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }

    public ProjectBaseModel getProject(){
        return project;
    }
}
